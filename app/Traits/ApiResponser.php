<?php 

namespace App\Traits;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\LengthAwarePaginator;

trait ApiResponser{

	private function successResponse($data, $code){
		$data = array(
			'status' => 'success', 
			'code'   => $code, 
			'data'   => $data, 
		);
		return response()->json($data, $code);
	}

	protected function errorResponse($message, $code){
		$data = array(
			'status' => 'error', 
			'code'   => $code, 
			'error'  => $message, 
		);
		return response()->json($data, $code);
	}

	protected function showAll(Collection $collection, $code = 200){
		 $transformer = $collection->first()->transformer;

		$collection = $this->transformData($collection, $transformer);
		return $this->successResponse($collection,$code);
	}

	protected function showOne(Model $instance, $code = 200){
		$transformer = $instance->transformer;
        $data = $this->transformData($instance, $transformer);
		return $this->successResponse($data,$code);
	}

    protected function transformData($data, $transformer){
        $transformation = fractal($data, new $transformer);

        return $transformation->toArray();
    }

}
