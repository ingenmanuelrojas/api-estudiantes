<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Country;
use App\Models\Course;

class Teacher extends Model
{
    //1 PROFESOR PERTENECE A UN PAIS
    public function country(){
    	return $this->belongsTo(Country::class);
    }

    //1 PROFESOR PUEDE DICTAR MUCHAS CURSOS
    public function courses(){
    	return $this->belongsToMany(Course::class);
    }
}
