<?php

namespace App\Models;

use App\Transformers\StudentTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Models\Country;
use App\Models\Course;
use Laravel\Passport\HasApiTokens;

class Student extends Model
{
    use HasApiTokens;
    
    protected $fillable = [
    	'name',
    	'surname',
    	'email',
    	'phone',
    	'address',
    	'country_id',
    ];

    public $transformer = StudentTransformer::class;
    
    //UN ESTUDIANTE SOLO POSEE UN PAIS, O UN ESTUDIANTE PERTENECE A UN PAIS
    public function country(){
    	return $this->belongsTo(Country::class);
    }

    //UN ESTUDIANTE PUEDE TENER MUCHAS MATERIAS/CURSOS
    public function courses(){
    	return $this->belongsToMany(Course::class);
    }
}
