<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Student;

class Section extends Model
{
    public function students(){
    	return $this->belongsToMany(Student::class);
    }

    public function course(){
    	return $this->belongsToMany(Student::class);
    }
}
