<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Student;
use App\Models\Teacher;

class Country extends Model
{

	//RELACION DE UNO A MUCHOS 
	//1 PAIS PUEDE TENER MUCHOS ESTUDIANTES
    public function students(){
    	return $this->hasMany(Student::class);
    }

    //RELACION DE UNO A MUCHOS 
	//1 PAIS PUEDE TENER MUCHOS PROFESORES
    public function teachers(){
    	return $this->hasMany(Teacher::class);
    }
}
