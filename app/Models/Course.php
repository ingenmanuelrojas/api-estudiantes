<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Student;
use App\Models\Teacher;
use App\Models\Section;

class Course extends Model
{
	//UN CURSO TIENE MUCHOS ESTUDIANTES
    public function students(){
    	return $this->belongsToMany(Student::class);
    }

    //UN CURSO PUEDE SER DICTADO POR 1 PROFESOR
    public function teacher(){
    	return $this->belongsTo(Teacher::class);
    }

    public function section(){
    	return $this->belongsToMany(Section::class);
    }
}
