<?php

namespace App\Exceptions;

use Exception;
use App\Traits\ApiResponser;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Auth\Access\AuthorizationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    use ApiResponser;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception){
        if ($exception instanceOf ValidationException) {
            return $this->convertValidationExceptionToResponse($exception, $request);
        }

        if ($exception instanceof AuthenticationException) {
            //Si la excepcion viene desde el login, se redirecciona d enuevo al login
            if ($this->isFrontend($request)) {
                return redirect()->guest('login');
            }else{//Sino quiere decir que es de la API y se retorna un json
                return $this->unauthenticated($request, $exception);
            }
        }

        if ($exception instanceof AuthorizationException) {
            return $this->errorResponse("No tiene permisos para ejecutar esta accion", 403);
        }

        if ($exception instanceof NotFoundHttpException) {
            return $this->errorResponse("No se encontro la URL especificada", 404);
        }

        //VALIDAR EXCEPCION PARA METODOS NO VALIDOS
        if ($exception instanceof MethodNotAllowedHttpException) {
            return $this->errorResponse("El método espécificado en la peticion no es válido", 405);
        }

        //VALIDAR EXCEPCION GENERAL PARA CUALQUIER PETICIONES HTTP
        if ($exception instanceof HttpException) {
            return $this->errorResponse($exception->getMessage(), $exception->getStatusCode());
        }

        return parent::render($request, $exception);

    }

    protected function convertValidationExceptionToResponse(ValidationException $e, $request){
        $errors = $e->validator->errors()->getMessages();

        return response()->json($errors, 422);
    }

    protected function unauthenticated($request, AuthenticationException $exception){
        return $this->errorResponse("No autenticado", 401);

    }

    private function isFrontend($request){
        //Verificar que es una ruta valida de las rutas web del proyecto frontend
        return $request->acceptsHtml() && collect($request->route()->middleware())->contains('web');
    }
       
}
