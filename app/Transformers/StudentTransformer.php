<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Student;

class StudentTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Student $student)
    {
        return [
            'identificador' => (int)$student->id, 
            'nombre'        => (string)$student->name, 
            'apellido'      => (string)$student->surname, 
            'correo'        => (string)$student->email, 
            'telefono'      => (string)$student->phone, 
            'direccion'     => (string)$student->address, 
            'fecha_creacion'     => $student->created_at, 
            'fecha_actualizacion'     => $student->updated_at,
        ];
    }
}
