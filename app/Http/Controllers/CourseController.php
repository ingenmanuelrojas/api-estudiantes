<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class CourseController extends ApiController
{

    public function index(){
        
    }

    public function store(Request $request){
        
    }

    public function show($id){
        
    }

    public function update(Request $request, $id){
        
    }

    public function destroy($id){
        
    }
}
