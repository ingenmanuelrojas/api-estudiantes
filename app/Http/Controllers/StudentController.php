<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\Country;
use App\Http\Controllers\ApiController;

class StudentController extends ApiController
{
	public function __construct()
	{
        $this->middleware('client.credentials')->only(['index', 'show']);
	}

    public function index(){
        $students = Student::all();
        return $this->showAll($students);
    }

    public function show(Student $student){
        return $this->showOne($student, 201); 
    }

    public function destroy($id){
        
    }
}
