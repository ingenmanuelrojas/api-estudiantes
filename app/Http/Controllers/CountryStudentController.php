<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\Student;
use App\Http\Controllers\ApiController;

class CountryStudentController extends ApiController
{

    public function __construct()
    {
        $this->middleware('auth:api')->only(['store']);
    }

    public function store(Request $request, Country $country){
    	$rules = [
    		'name'    => 'required',
    		'surname' => 'required',
    		'email'   => 'required|email|unique:students',
    	];

    	$this->validate($request, $rules);

    	$campos = $request->all();
    	$campos['country_id'] = $country->id;

    	$student = Student::create($campos);

    	return $this->showOne($student, 201);
    }
}
