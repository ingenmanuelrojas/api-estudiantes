<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/


/*RUTAS COUNTRIES*/
Route::resource('countries', 'CountryController', ['only'   => ['index', 'show']]);
Route::resource('countries.teachers', 'CountryTeacherController', ['only' => ['index']]);
Route::resource('countries.students', 'CountryStudentController', ['only' => ['index','store','update']]);

/*RUTAS STUDENTS*/
Route::resource('students', 'StudentController',  ['only' => ['index','show']]);

/*RUTAS COURSES*/
Route::resource('courses', 'CourseController',    ['except' => ['create', 'edit']]);

/*RUTAS COURSES*/
Route::resource('teachers', 'TeacherController',  ['except' => ['create', 'edit']]);
Route::resource('teachers.courses', 'TeacherCourseController',  ['only' => ['index']]);

Route::post('oauth/token', '\Laravel\Passport\Http\Controllers\AccessTokenController@issueToken');




