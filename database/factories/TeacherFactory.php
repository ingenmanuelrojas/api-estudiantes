<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Country;
use App\Models\Teacher;
use Faker\Generator as Faker;

$factory->define(Teacher::class, function (Faker $faker) {
	$countries = Country::all()->random();

    return [
        'name'    => strtoupper($faker->name),
        'surname' => strtoupper($faker->lastName),
        'email'   => $faker->unique()->freeEmail,
        'profession' => $faker->jobTitle,
        'phone'   => $faker->phoneNumber,
        'address' => $faker->address,
        'country_id' => $countries->id,
    ];
});