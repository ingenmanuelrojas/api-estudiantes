<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Student;
use App\Models\Course;
use App\Models\Section;
use Faker\Generator as Faker;

$factory->define(Section::class, function (Faker $faker) {
	$students = Student::all()->random();
	$courses  = Course::all()->random();

    return [
        'student_id' => $students->id,
        'course_id'  => $courses->id,
        'classroom'  => $faker->buildingNumber,
    ];
});