<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Student;
use App\Models\Country;
use Faker\Generator as Faker;

$factory->define(Student::class, function (Faker $faker) {
	$countries = Country::all()->random();

    return [
        'name'    => strtoupper($faker->name),
        'surname' => strtoupper($faker->lastName),
        'email'   => $faker->unique()->freeEmail,
        'phone'   => $faker->phoneNumber,
        'address' => $faker->address,
        'country_id' => $countries->id,
    ];
});