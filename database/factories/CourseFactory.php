<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Course;
use App\Models\Teacher;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
	$teachers = Teacher::all()->random();

    return [
        'name'       => $faker->text($maxNbChars = 15),
        'teacher_id' => $teachers->id,
    ];
});
