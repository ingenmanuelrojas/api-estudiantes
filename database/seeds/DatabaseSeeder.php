<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Student;
use App\Models\Course;
use App\Models\Teacher;
use App\Models\Section;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	$this->truncateTablas([
            'users',
            'students',
            'teachers',
            'courses',
            'sections',
        ]);

    	//EVITAR QUE SE EJECUTEN LOS EVENTOS PREDETERMINADOS DE LOS MODELOS
        Student::flushEventListeners();
        Course::flushEventListeners();
        Teacher::flushEventListeners();
        User::flushEventListeners();
        Section::flushEventListeners();

        $this->call(UsersTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(StudentsTableSeeder::class);
        $this->call(TeachersTableSeeder::class);
        $this->call(CoursesTableSeeder::class);
        $this->call(SectionsTableSeeder::class);
    }

    protected function truncateTablas(array $tablas){
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        foreach ($tablas as $tabla) {
            DB::table($tabla)->truncate();
        }
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    }
}
